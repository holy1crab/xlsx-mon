from enum import Enum
from time import time
from os.path import join
from datetime import datetime
from openpyxl import Workbook
from openpyxl.utils import get_column_letter
from openpyxl.styles import PatternFill

from config import document_dir, server_structure


class ServiceState(Enum):
    Ok = 0
    Warning = 10
    Bad = 20


class ServiceStatus:

    def __init__(self):
        self.created_timestamp = time()
        self.state = None


wb = Workbook()
ws_active = wb.active
ws_active.title = 'Everyday report'


def get_service_depth(service):
    arr = service['children'][:] if 'children' in service else []
    depth = 0

    while len(arr):

        el = arr.pop(0)

        if 'children' in el:
            arr = el['children'] + arr

        if 'services' in el:
            depth += len(el['services'])

    return depth


def generate_file():
    titles = [
        'Сервис',
        'Стенд',
        'Хост'
    ]

    row_offset = 1

    for index, title in enumerate(titles, 1):
        _ = ws_active.cell(1, index, title)
        # cell.fill = PatternFill(bgColor='FFC7CE', fill_type='solid')
        # cell.fill = PatternFill('solid', bgColor='DDDDDD')

    ws_active.merge_cells(start_row=1, end_row=1, start_column=3, end_column=4)

    row_offset += 1

    write_services_rec(server_structure, row_offset, 1)
    # for index, el in enumerate(server_structure):
    #     column_index = 1

    adjust_column_width()

    wb.save(join(document_dir, 'output_{}.xlsx'.format(datetime.now().strftime('%d_%m_%Y'))))


def adjust_column_width():
    for col in ws_active.columns:
        column = col[0].column
        ws_active.column_dimensions[column].width = 30


def write_services_rec(services, row, column):
    for index, service in enumerate(services):

        service_depth = get_service_depth(service)

        title = service.get('title')
        print(title, service_depth, row)

        if 'services' in service:
            services_count = len(service['services'])

            if services_count == 1:
                ws_active.merge_cells(start_row=row, end_row=row, start_column=column, end_column=column + 1)

        cell = ws_active.cell(row, column, title)

        if service_depth > 0:
            ws_active.merge_cells(start_row=row, end_row=row + service_depth - 1,
                                  start_column=column, end_column=column)

        if 'children' in service:
            write_services_rec(service['children'], row, column + 1)
        elif 'services' in service:
            write_services_rec(service['services'], row, column + 1)
            row += len(service['services']) - 1

        row += 1 if service_depth == 0 else service_depth


def main():
    generate_file()


if __name__ == '__main__':
    main()
