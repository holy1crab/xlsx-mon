from os.path import dirname, join
from os import environ
from dotenv import load_dotenv

root_dir = dirname(dirname(__file__))

load_dotenv(join(root_dir, '.env'))

document_dir = join(root_dir, 'dist')

zabbix_host = environ.get('ZABBIX_HOST', 'http://localhost:8081')
zabbix_username = environ.get('ZABBIX_USERNAME', 'Admin')
zabbix_password = environ.get('ZABBIX_PASSWORD', 'zabbix')

monitoring_hosts = environ.get('MONITORING_HOSTS', '')
monitoring_hosts = monitoring_hosts.replace(';', ',').split(',')

server_structure = [
    {
        'title': 'Централизованные nginx',
        'children': [
            {
                'title': 'DEV',
                'children': [
                    {
                        'title': 'sbt-orefs-010.ca.sbrf.ru',
                        'services': [
                            {
                                'host': 'sbt-orefs-010.ca.sbrf.ru',
                                'service': ''
                            }
                        ]
                    }
                ],
            },
            {
                'title': 'MMV',
                'children': [
                    {
                        'title': 'sbt-orefs-006.ca.sbrf.ru',
                        'services': [
                            {
                                'host': 'sbt-orefs-006.ca.sbrf.ru',
                                'service': ''
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        'title': 'Централизованные MQ',
        'children': [
            {
                'title': 'DEV',
                'children': [
                    {
                        'title': 'sbt-orefs-042.ca.sbrf.ru',
                        'services': [
                            {
                                'host': 'sbt-orefs-042.ca.sbrf.ru',
                                'service': ''
                            }
                        ]
                    },
                    {
                        'title': 'sbt-orefs-039.ca.sbrf.ru',
                        'services': [
                            {
                                'host': 'sbt-orefs-039.ca.sbrf.ru',
                                'service': ''
                            }
                        ]
                    }
                ]
            },
            {
                'title': 'MMV',
                'children': [
                    {
                        'title': 'sbt-orefs-009.ca.sbrf.ru',
                        'services': [
                            {
                                'host': 'sbt-orefs-009.ca.sbrf.ru',
                                'service': ''
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        'title': 'Базы данных',
        'children': [
            {
                'title': 'DEV',
                'children': [
                    {
                        'title': 'sbt-orefs-056.ca.sbrf.ru',
                        'services': [
                            {
                                'title': 'DEV27',
                                'host': 'sbt-orefs-056.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'DEV28',
                                'host': 'sbt-orefs-056.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'DEV181',
                                'host': 'sbt-orefs-056.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'DAILY27',
                                'host': 'sbt-orefs-056.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'DAILY28',
                                'host': 'sbt-orefs-056.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'DAILY181',
                                'host': 'sbt-orefs-056.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'UFSIFT',
                                'host': 'sbt-orefs-056.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'PRIMARY',
                                'host': 'sbt-orefs-056.ca.sbrf.ru',
                                'service': ''
                            },
                        ]
                    },
                    {
                        'title': 'sbt-orefs-046.ca.sbrf.ru',
                        'services': [
                            {
                                'title': 'SI26',
                                'host': 'sbt-orefs-046.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'TEST28',
                                'host': 'sbt-orefs-046.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'STANDIN',
                                'host': 'sbt-orefs-046.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'NIGHTLY',
                                'host': 'sbt-orefs-046.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'SITEST26',
                                'host': 'sbt-orefs-046.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'SIDAIL26',
                                'host': 'sbt-orefs-046.ca.sbrf.ru',
                                'service': ''
                            },
                        ]
                    },
                    {
                        'title': 'sbt-orefs-041.ca.sbrf.ru',
                        'services': [
                            {
                                'title': 'FRESH',
                                'host': 'sbt-orefs-041.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'TEST27',
                                'host': 'sbt-orefs-041.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'REPDEV',
                                'host': 'sbt-orefs-041.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'FRESHSND',
                                'host': 'sbt-orefs-041.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'FRESHTST',
                                'host': 'sbt-orefs-041.ca.sbrf.ru',
                                'service': ''
                            },
                        ]
                    },
                    {
                        'title': 'sbt-ouiefs-0098.ca.sbrf.ru',
                        'services': [
                            {
                                'title': 'TEST181',
                                'host': 'sbt-orefs-0098.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'INTDEV',
                                'host': 'sbt-orefs-0098.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'SBOL',
                                'host': 'sbt-orefs-0098.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'SBOLADM',
                                'host': 'sbt-orefs-0098.ca.sbrf.ru',
                                'service': ''
                            }
                        ]
                    }
                ]
            },
            {
                'title': 'MMV',
                'children': [
                    {
                        'title': 'sbt-orefs-036.ca.sbrf.ru',
                        'services': [
                            {
                                'title': 'STABDEV',
                                'host': 'sbt-orefs-036.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'STABLOG',
                                'host': 'sbt-orefs-036.ca.sbrf.ru',
                                'service': ''
                            },
                            {
                                'title': 'ST',
                                'host': 'sbt-orefs-036.ca.sbrf.ru',
                                'service': ''
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        'title': 'Централизованные XS',
        'children': [
            {
                'title': 'DEV',
                'children': [
                    {
                        'title': 'sbt-ouiefs-0103.ca.sbrf.ru:4811',
                        'services': [
                            {
                                'host': 'sbt-ouiefs-0103.ca.sbrf.ru:4811',
                                'service': ''
                            }
                        ]
                    },
                    {
                        'title': 'sbt-ouiefs-0102.ca.sbrf.ru:4811',
                        'services': [
                            {
                                'host': 'sbt-ouiefs-0102.ca.sbrf.ru:4811',
                                'service': ''
                            }
                        ]
                    },
                    {
                        'title': 'sbt-orefs-039.ca.sbrf.ru:4812',
                        'services': [
                            {
                                'host': 'sbt-orefs-039.ca.sbrf.ru:4812',
                                'service': ''
                            }
                        ]
                    },
                    {
                        'title': 'sbt-orefs-039.ca.sbrf.ru:4813',
                        'services': [
                            {
                                'host': 'sbt-orefs-039.ca.sbrf.ru:4813',
                                'service': ''
                            }
                        ]
                    }
                ],
            },
            {
                'title': 'MMV',
                'children': [
                    {
                        'title': 'sbt-orefs-006.ca.sbrf.ru',
                        'services': [
                            {
                                'host': 'sbt-orefs-006.ca.sbrf.ru',
                                'service': ''
                            }
                        ]
                    }
                ]
            }
        ]
    },
    {
        'title': 'Jenkins',
        'children': [
            {
                'title': 'SLAVES',
                'children': [
                    {
                        'title': 'sbt-ouiefs-023.ca.sbrf.ru',
                        'services': [
                            {
                                'host': 'sbt-ouiefs-023.ca.sbrf.ru',
                                'service': ''
                            }
                        ]
                    },
                    {
                        'title': 'sbt-ouiefs-024.ca.sbrf.ru',
                        'services': [
                            {
                                'host': 'sbt-ouiefs-024.ca.sbrf.ru',
                                'service': ''
                            }
                        ]
                    },
                    {
                        'title': 'sbt-ouiefs-0112.ca.sbrf.ru',
                        'services': [
                            {
                                'host': 'sbt-ouiefs-0112.ca.sbrf.ru',
                                'service': ''
                            }
                        ]
                    },
                    {
                        'title': 'sbt-ouiefs-0116.ca.sbrf.ru',
                        'services': [
                            {
                                'host': 'sbt-ouiefs-0116.ca.sbrf.ru',
                                'service': ''
                            }
                        ]
                    }
                ]
            }
        ]
    }
]
