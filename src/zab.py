# -*- coding: utf8 -*-
import sys
import os
import logging
from pprint import pprint
from time import time
from os.path import join
from datetime import datetime

from pyzabbix import ZabbixAPI
from openpyxl import Workbook
from openpyxl.styles import PatternFill, Font

from config import zabbix_username, zabbix_password, zabbix_host, document_dir, monitoring_hosts

# stream = logging.StreamHandler(sys.stdout)
# stream.setLevel(logging.DEBUG)
# log = logging.getLogger('pyzabbix')
# log.addHandler(stream)
# log.setLevel(logging.DEBUG)


zapi = ZabbixAPI(zabbix_host)

# zapi.session.auth = (zabbix_username, zabbix_password)
zapi.session.verify = False
zapi.timeout = 10

EVENT_SOURCE_TRIGGER = '0'
FILL_SUCCESS = PatternFill(start_color='b8f3b8', end_color='b8f3b8', fill_type='solid')
FILL_FAILURE = PatternFill(start_color='fb9c9c', end_color='fb9c9c', fill_type='solid')
FILL_HEADER = PatternFill(start_color='565656', end_color='565656', fill_type='solid')


def timestamp_format(x):
    return datetime.fromtimestamp(x).strftime('%Y-%m-%d %H:%M:%S')


class Row:
    def __init__(self):
        self.host = None
        self.description = None
        self.event_start_time = None
        self.event_end_time = None


class XlsxOutput:

    def __init__(self, results, filepath):
        self.results = results
        self.filepath = filepath

    def output(self):
        wb = Workbook()

        ws_active = wb.active
        ws_active.title = 'Everyday report'

        headers = [
            'Host',
            'Description',
            'Start time',
            'End time',
            'Duration'
        ]

        row_index = 1

        font = Font(color='FFFFFF')

        for index, header in enumerate(headers, 1):
            cell = ws_active.cell(row_index, index, header)
            cell.fill = FILL_HEADER
            cell.font = font

        for index, el in enumerate(results):
            row_index += 1

            host = el.host
            start_time_str = timestamp_format(el.event_start_time) if el.event_start_time is not None else ''
            description_str = el.description if el.description is not None else 'OK'

            host_cell = ws_active.cell(row_index, 1, host)
            description_cell = ws_active.cell(row_index, 2, description_str)
            start_time_cell = ws_active.cell(row_index, 3, start_time_str)

            if el.description is None:
                description_cell.fill = FILL_SUCCESS
            else:
                description_cell.fill = FILL_FAILURE

            if el.event_end_time is not None:
                ws_active.cell(row_index, 4, timestamp_format(el.event_end_time))
                ws_active.cell(row_index, 5, el.event_end_time - el.event_start_time)

        wb.save(self.filepath)


class HtmlOutput:

    def __init__(self, results, filepath):
        self.results = results
        self.filepath = filepath

    def output(self):
        content = """<!doctype html>
<html>
<body>
    <table>
        <tbody>
            {0}
        </tbody>
    </table>
</body>
</html>"""

        rows = []

        for el in results:
            rows.append("""<tr>
            <td>{0}</td>
            <td>{1}</td>
            <td>{2}</td>
            </tr>
            """.format(
                el.host,
                el.description if el.description is not None else 'OK',
                timestamp_format(el.event_start_time) if el.event_start_time is not None else ''),
            )

        content = content.format(''.join(rows))

        with open(self.filepath, 'w+') as f:
            f.write(content)


results = []


def handle_host(host):
    now = round(time())

    host_id = host.get('hostid')
    host_name = host.get('host')

    events = zapi.do_request('event.get', {
        'source': EVENT_SOURCE_TRIGGER,
        'time_from': str(now - 7 * 24 * 3600),
        'time_till': str(now),
        'hostids': [host_id],
        # 'selectHosts': 'extended',
    }).get('result')

    trigger_ids = set()
    id_to_event = {}

    for event in events:
        trigger_ids.add(event['objectid'])
        id_to_event[event['eventid']] = event

    triggers = zapi.do_request('trigger.get', {'triggerids': list(trigger_ids)}).get('result')

    id_to_trigger = dict(map(lambda el: (el.get('triggerid'), el), triggers))

    has_events = False

    for event in events:
        if event['value'] != '1':
            continue

        has_events = True

        # pprint(event)

        recovery_event_id = event.get('r_eventid')

        event_end_time = None

        if recovery_event_id != '0':
            recovery_event = id_to_event.get(recovery_event_id)
            if recovery_event is not None:
                event_end_time = int(recovery_event.get('clock'))

        trigger_id = event.get('objectid')
        trigger = id_to_trigger[trigger_id]

        # print('trigger ->', trigger)

        description = trigger.get('description')
        event_time = event.get('clock')

        row = Row()

        row.host = host_name
        row.event_start_time = int(event_time)
        row.description = description
        row.event_end_time = event_end_time

        results.append(row)

    if not has_events:
        row = Row()

        row.host = host_name
        row.event_start_time = None
        row.description = None
        row.event_end_time = None

        results.append(row)


def main():
    zapi.login(zabbix_username, zabbix_password)

    hosts = zapi.do_request('host.get', {'filter': {'host': monitoring_hosts}}).get('result')

    for host in hosts:
        print(host['host'])
        handle_host(host)

    # output = HtmlOutput(results, join(document_dir, 'report.html'))
    # output.output()

    xlsx_output = XlsxOutput(results, join(document_dir, 'report.xlsx'))
    xlsx_output.output()


if __name__ == '__main__':
    main()
